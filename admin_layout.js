'use strict';

angular.module('amt.adminLayout', ['ui.bootstrap', 'restangular']).
    factory('currentUser', function ($http) {
        var promise
        return {
            get_async : function(){
                if(!promise){
                    promise = $http.get('/_admin/current_user').then(function (response) {
                        // The then function here is an opportunity to modify the response
                        console.log(response);
                        // The return value gets picked up by the then in the controller.
                        return {
                            data : response.data,
                            has_role: function(role){
                                return this.data.roles.indexOf(role) > -1
                            },
                            has_action: function(action){
                                return this.data.actions.indexOf(action) > -1
                            }
                        };
                    });
                }
                return promise
            }
        }
    }).

    provider('adminMenu', function(){
        var menu = {
            menu_items:{},
            current_user:undefined
        };
        var active_endpoint = undefined;
        var href_to_menu = {};
        href_to_menu["/"] = menu.menu_items.home;

        this.get_menu_item = function(endpoint){
            var menu_item = menu;
            if(endpoint != undefined && endpoint != '.' && endpoint != ''){
                var search_endpoint;
                var path = endpoint.split('.')
                var i;
                for(i in path ){
                    search_endpoint = path[i]
                    if(menu_item.menu_items[search_endpoint] != undefined){
                        menu_item = menu_item.menu_items[search_endpoint]
                    }
                }
            }
            return menu_item
        };

        function set_active_state_menu_item(menu_item, state){
            menu_item.active = state;
            var parent = menu_item.parent;
            while(parent != undefined){
                parent.active = state;
                parent = parent.parent;
            }
        }

        this.add_menu_item = function(endpoint, title, href, parent_endpoint){
            var menu_item = this.get_menu_item(parent_endpoint)
            menu_item.menu_items[endpoint] = {
                active: false,
                endpoint: endpoint,
                title: title,
                href:  "#" + href,
                menu_items: {},
                parent:menu_item
            }
            href_to_menu[href] = menu_item.menu_items[endpoint]
        };

        this.set_active_endpoint = function(endpoint){
            var menu_item = this.get_menu_item(endpoint)
            if(active_endpoint != undefined){
                var active_menu_item = this.get_menu_item(endpoint)
                set_active_state_menu_item(active_menu_item, false)
            }
            active_endpoint = endpoint
            set_active_state_menu_item(menu_item, true)
        };
        this.set_active_href = function(href){
            if(href in href_to_menu){
                this.set_active_endpoint(href_to_menu[href].endpoint)
            }
        };

        this.get_menu = function(locationPath){
            this.set_active_href(locationPath)
            return menu
        };

        this.$get = function() {
            return this
        };

    }).
    provider('adminMenuBrand', function(){
        var brand = {
            name: "Set Name",
            image_url: "brand_icon.png",
            href:"/"
        };
        this.set_brand = function(name, image_url, href){
            brand.name = name
            brand.image_url = image_url
            brand.href = href
        };
        this.get_brand = function(){
            return brand
        };

        this.$get = function() {
            return this
        };
    }).

    directive('menu',['$location', 'adminMenu', 'adminMenuBrand', 'currentUser',
        function(location, adminMenu, adminMenuBrand, currentUser) {
        var html = '' +
            '<div class="container-fluid">' +
            '   <div class="navbar-header">' +
            '       <button type="button" class="navbar-toggle" ' +
            '           data-toggle="collapse" ' +
            '           data-target="#bs-example-navbar-collapse-1">'+
            '           <span class="sr-only">Toggle navigation</span>' +
            '           <span class="icon-bar"></span>' +
            '           <span class="icon-bar"></span>' +
            '           <span class="icon-bar"></span>' +
            '       </button>'+
            '       <a class="navbar-brand" href="{{ brand.href }}" style="padding: 0px 10px 0px 10px;">' +
            '           <img src="{{ brand.image_url }}" height="50px" width="50px">' +
            '       </a>' +
            '   </div>' +
            '   <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">' +
            '       <ul class="nav navbar-nav" ng-if="current_user">' +
            '           <li ng-repeat="(item_endpoint, item) in menu.menu_items" menu-item="item"></li>' +
            '       </ul>' +
            '       <ul class="nav navbar-nav navbar-right">' +
            '           <li><a href="#"><img src="images/spinner.gif" ng-show="has_pending_request()"/></a></li>' +
            '           <li class="dropdown" ng-if="current_user">' +
            '               <a class="dropdown-toggle" data-toggle="dropdown">' +
            '                   {{current_user.data.email}}<b class="caret"></b>' +
            '               </a>' +
            '               <ul class="dropdown-menu">' +
            '                   <li><a href="{{current_user.data.logout_url}}">Logout</a></li>' +
            '                   <li ng-if="current_user.has_role(\'admin\')">' +
            '                       <a href="#/user_manager/">Manage Users</a>' +
            '                   </li>' +
            '               </ul>' +
            '           </li>' +
            '       </ul>' +
            '   </div>' +
            '</div>';
        return {
            restrict: 'A',
            scope: {
                menu: '=menu'
            },

            template: html,
            link: function(scope, element, attrs) {
                element.addClass(attrs.class);
                element.addClass(scope.cls);
                scope.$location = location;
                scope.$watch('$location.path()', function(locationPath) {
                    adminMenu.set_active_href(locationPath)
                });
            },
            controller: ['$scope', '$http', function adminMenuCtrl( $scope, $http){
                $scope.menu = adminMenu.get_menu()
                $scope.brand = adminMenuBrand.get_brand()
                $scope.current_user = undefined;
                currentUser.get_async().then(function(current_user){
                    $scope.current_user = current_user
                });
                $scope.user_has_role = currentUser.has_role
                $scope.has_pending_request = function() {
                    return $http.pendingRequests.length > 0;
                };
                console.log($scope.menu)
            }]
        };
    }]).

    directive('menuItem', function($compile) {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                item: '=menuItem'
            },
            template: '<li active-link><a href={{item.href}}>{{item.title}}</a></li>',
            link: function (scope, element, attrs) {
                if (scope.item.header) {
                    element.addClass('nav-header');
                    element.text(scope.item.header);
                }
                if (scope.item.divider) {
                    element.addClass('divider');
                    element.empty();
                }
                if (Object.keys(scope.item.menu_items).length) {
                    element.addClass('dropdown');

                    var text = element.children('a').text();
                    element.empty();
                    var $a = $('<a class="dropdown-toggle">'+text+'</a>');
                    element.append($a);

                    var $submenu = $('' +
                        '<ul class="dropdown-menu">' +
                        '   <li ng-repeat="(item_endpoint, item) in item.menu_items" menu-item="item">' +
                        '   </li>' +
                        '</ul>');
                    element.append($submenu);
                }
                if (scope.item.click) {
                    element.find('a').attr('ng-click', 'item.click()');
                }
                $compile(element.contents())(scope);
            }
        };
    }).

    controller('UserProfileCtrl', [
        '$scope', '$location', '$route', 'currentUser', 'user', 'role_items', 'UserManagerRest',
        function ($scope, $location, $route, currentUser, user, role_items, UserManagerRest) {
            currentUser.get_async().then(function(current_user){
                $scope.current_user = current_user
            });
            $scope.user = user;
            console.log(user)
            $scope.role_items = role_items;
            $scope.toggle_role = function (role) {
                var action = $scope.user.roles.indexOf(role) > -1 ? "remove_role" : "add_role";
                UserManagerRest.one('users', $scope.user.id).customPOST({role: role }, action).then(function (obj) {
                    $scope.user = obj
                })
            };
            $scope.toggle_role_action = function (role_action) {
                console.log(role_action)
                var action = $scope.user.actions.indexOf(role_action) > -1 ? "remove_role_action" : "add_role_action";
                UserManagerRest.one('users', $scope.user.id).customPOST({role_action: role_action }, action).then(function (obj) {
                    $scope.user = obj
                })
            }
    }]).

    controller('UserMangerCtrl', [
        '$scope', '$location', '$route', 'currentUser', 'user_items',
        function ($scope, $location, $route, currentUser, user_items) {
            currentUser.get_async().then(function(current_user){
                $scope.current_user = current_user
            });
            $scope.user_items = user_items;
            $scope.isNotCurrentUser = function (user) {
                return $scope.current_user.data.id !== user.id
            };
            $scope.isNotAdminRole = function (role) {
                return role !== 'admin'
            };
            $scope.hasAdminRole = function (user) {
                return user.roles.indexOf('admin') > -1
            };
            $scope.toggle_admin_confirmed = function (user_id) {
                var user = _.find($scope.user_items, function (obj) {
                    return obj.id === user_id
                });
                user.admin_confirmed = !user.admin_confirmed;
                user.put()
            };
            $scope.delete_user = function (user_id) {
                var user = _.find($scope.user_items, function (obj) {
                    return obj.id === user_id
                });
                if (!confirm('Delete user : ' + user.email + ' ?')) {
                    return
                }
                user.remove().then(function (obj) {
                    $scope.user_items = _.reject($scope.user_items, {id: obj.id})
                })
            };
    }]).

    factory('UserManagerRest', ['$notification', 'Restangular', function ($notification, Restangular) {
        return Restangular.withConfig(function (config) {
            config.setBaseUrl('/_admin/users_manager/');
            config.setRequestInterceptor(function (elem, operation) {
                if (operation === "remove") {
                    return undefined;
                }
                return elem;
            });
            config.setErrorInterceptor(
                function(resp) {
                    console.log(resp)
                    if(resp.status == 403){
                        $notification.warning("Access Denied", resp.config.url)
                    }else{
                        $notification.error("Server Error", resp.data.error)
                    }

                    return false; // stop the promise chain
                });
        });
    }])

    .config(function ($routeProvider) {
        $routeProvider.
            when('/user_manager', {
                templateUrl: 'bower_components/admin_layout/user_manager.html',
                controller: 'UserMangerCtrl',
                resolve: {
                    user_items: ['UserManagerRest', function (UserManagerRest) {
                        return UserManagerRest.all('users').getList();
                    }]
                }
            }).
            when('/user_manager/:user_id', {
                templateUrl: 'bower_components/admin_layout/user_profile.html',
                controller: 'UserProfileCtrl',
                resolve: {
                    user: ['UserManagerRest', '$route', function (UserManagerRest, $route) {
                        var params = $route.current.params;
                        return UserManagerRest.one('users', params.user_id).get();
                    }],
                    role_items: ['UserManagerRest', function (UserManagerRest) {
                        return UserManagerRest.all('roles').getList();
                    }]
                }
            });

    });